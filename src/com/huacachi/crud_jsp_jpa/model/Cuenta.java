package com.huacachi.crud_jsp_jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cuentas")
public class Cuenta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cue_codigo")
	private int codigo;
	
	@Column(name = "cue_descripcion")
	private String descripcion;
	
	@Column(name = "cue_titular")
	private String titular;
	
	@ManyToOne
	@JoinColumn(name = "cue_banco")
	private Banco banco;
	
	@ManyToOne
	@JoinColumn(name = "cue_moneda")
	private Moneda moneda;
	
	public Cuenta() {
		// TODO Auto-generated constructor stub
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Moneda getMoneda() {
		return moneda;
	}

	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}

	@Override
	public String toString() {
		return "Cuenta [codigo=" + codigo + ", descripcion=" + descripcion + ", titular=" + titular + ", banco=" + banco
				+ ", moneda=" + moneda + "]";
	}
	
	
}
