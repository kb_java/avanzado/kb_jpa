package com.huacachi.crud_jsp_jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "alumnos")
@PrimaryKeyJoinColumn(referencedColumnName = "alu_persona")
public class Alumno extends Persona{
	@Column(name = "alu_semestre")
	private String semestre;
	
	public Alumno() {
		// TODO Auto-generated constructor stub
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	@Override
	public String toString() {
		return "Alumno [semestre=" + semestre + "]";
	}
	
	
}
