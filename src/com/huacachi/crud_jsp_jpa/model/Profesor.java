package com.huacachi.crud_jsp_jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "profesores")
@PrimaryKeyJoinColumn(referencedColumnName = "pro_persona")
public class Profesor extends Persona{
	@Column(name = "pro_especialidad")
	private String especialidad;
	
	public Profesor() {
		// TODO Auto-generated constructor stub
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	@Override
	public String toString() {
		return "Profesor [especialidad=" + especialidad + "]";
	}
	
	
}
