package com.huacachi.crud_jsp_jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bancos")
public class Banco {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ban_codigo")
	private int codigo;
	
	@Column(name = "ban_nombre")
	private String nombre;
	
	@OneToMany(mappedBy = "banco")
	private List<Cuenta> cuentas;
	
	public Banco() {
		// TODO Auto-generated constructor stub
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	@Override
	public String toString() {
		return "Banco [codigo=" + codigo + ", nombre=" + nombre + ", cuentas=" + cuentas + "]";
	}
	
	

}
